/**
 * WordPress dependencies
 */

 import { __ } from '@wordpress/i18n';
 import { MediaUpload, InnerBlocks, useBlockProps } from '@wordpress/block-editor';
 import { Button } from '@wordpress/components';
 
 const Edit = ( props ) => {
	 const {
		 attributes: { mediaID, mediaURL },
		 setAttributes,
	 } = props;	
 
	 const blockProps = useBlockProps();
 
	 const onSelectImage = ( media ) => {
		 setAttributes( {
			 mediaURL: media.url,
			 mediaID: media.id,
		 } );
	 };
 
 
	 console.log( blockProps );
 
	 return (
		 <div { ...blockProps }>
 
			 <div className="container-fluid position-relative">
 
				 <div className="row">
 
					 <div className="container">
 
						 <div className="row">
 
							 <div className="col-12 col-md-6 left-half">
 
								 <InnerBlocks />
 
							 </div>
 
							 <div className="col-12 col-md-6 right-half">
 
								 <MediaUpload
									 onSelect={ onSelectImage }
									 allowedTypes="image"
									 value={ mediaID }
									 render={ ( { open } ) => (
										 <Button
											 className={
												 mediaID ? 'image-button' : 'button button-large'
											 }
											 onClick={ open }
										 >
											 { ! mediaID ? (
												 __( 'Upload Image', 'gutenberg-examples' )
											 ) : (
												 <img
													 className='background_images'
													 src={ mediaURL }
													 alt={ __(
														 'Upload Recipe Image',
														 'gutenberg-examples'
													 ) }
												 />
											 ) }
										 </Button>
									 ) }
								 />
 
							 </div>
 
						 </div>
 
					 </div>
 
				 </div>
 
			 </div>
 
		 </div>
	 );
 };
 export default Edit;
 